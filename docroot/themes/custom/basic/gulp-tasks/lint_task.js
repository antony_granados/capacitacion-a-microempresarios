module.exports = function (gulp, plugins) {
  let notIncludeTheseFiles = [
    '!./assets/js/components/index.js',
    '!./assets/js/util/index.js',
    '!./assets/js/vendor/**/**',
    '!./assets/js/components.js',
    '!./assets/js/util.js',
    '!./assets/js/vendor.js',
    '!./assets/js/util/is_mobile.js',
    '!./assets/js/components/where_to_buy.js',
    ];
  let filesToLint = ['./assets/js/**/*.js'];
  return function () {
    gulp.src('filesToLint.concat(notIncludeTheseFiles')
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format())
        .pipe(plugins.eslint.failOnError())
  };
};